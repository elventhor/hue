<?php
function debug($data) {
	if(is_array($data)) {
		$data = print_r($data,1);
	}
	if(is_numeric($data)) { 
		$data = (string)$data;
	}
	if(!is_string($data)) { $data = serialize($data); }
	$data = date('c').' | '.trim($data)."\n";
	echo $data;
}
function getLightList() {
	$index = getLightIndex();
	foreach($index as $light) {
		$names[] = $light['name'];
	}
	return $names;
}
function getLightIndex() {
	global $config;
	global $index;
	$data = json_decode(file_get_contents($config['url'].'lights'),1);
	foreach($data as $k => $light) {
		$index[$k] = $light;
	}
	debug("Got an index of ".count($index)." lights");
	return $index;
}
function getidbyname($name)
{
	global $index;
	if(empty($index)) {
		$index = getLightIndex();
	}
	foreach($index as $k => $v) {
		if($v['name'] == $name) {
			debug("Found id $k for light $name");
			return($k);
		}
	}
	debug("Did not find id for light $name");
}
function clean($lights) {
	if($lights[0] == "all") {
		$index = getLightIndex();
		return(array_keys($index));
	}
	if(!is_array($lights)) {
		$lights2[] = $lights;
	} else {
		$lights2 = $lights;
	}
	foreach($lights2 as $light) {
		if(is_int($light)) {
			$lights3[] = $light;
		} else {
			$lights3[] = getidbyname($light);
		}
	}
	print_r($lights3);
	return $lights3;
}
function turnon($lights)
{
	$lights = clean($lights);
        foreach($lights as $l) {
                put('lights/'.$l.'/state',array('on' => true) );
        }

}
function turnoff($lights)
{
	$lights = clean($lights);
	print_r($lights);
	foreach($lights as $l) {
		put('lights/'.$l.'/state',array('on' => false) );
	}
}
function put($command, $data) {
	global $config;
	$url = $config['url'].$command;
	debug($url);
	$data_json = json_encode($data);
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);

        $response = curl_exec($ch);
        if(!$response) {
            return false;
        } 
	return $response;
}
